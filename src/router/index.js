//import libraries
import Vue from "vue";
import VueRouter from "vue-router";
import firebase from "firebase"
//import views
import Main from "../views/Main.vue";
import Deposit from "../views/Deposit.vue";
import Transfer from "../views/Transfer.vue";
import Withdraw from "../views/Withdraw.vue";
import Login from "../views/Login";
import Transactions from "../views/Transactions";
/////////////////////////////////////////////
import UnitTest from "../views/UnitTest.vue";

Vue.use(VueRouter);

const routes = [{
        path: "/login",
        name: "Авторизация",
        component: Login
    },
    {
        path: "/wallet",
        name: "Кошелёк",
        component: Main,
        meta: {
            requiresAuth: true
        }
    },
    {
        path: "/deposit",
        name: "Пополнение",
        component: Deposit,
        props: true,
        meta: {
            requiresAuth: true
        }
    },
    {
        path: "/transfer",
        name: "Переводы",
        component: Transfer,
        props: true,
        meta: {
            requiresAuth: true
        }
    },
    {
        path: "/withdraw",
        name: "Вывод",
        component: Withdraw,
        props: true,
        meta: {
            requiresAuth: true
        }
    },
    {
        path: "/test",
        name: "Test API",
        component: UnitTest,
        props: true,
        meta: {
            requiresAuth: true
        }
    },
    {
        path: "/transactions",
        name: "Транзакции",
        component: Transactions,
        props: true,
        meta: {
            requiresAuth: true
        }
    }
];

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes
});

router.beforeEach((to, from, next) => {
    const currentUser = firebase.auth().currentUser;
    const requiresAuth = to.matched.some(record => record.meta.requiresAuth);

    if (requiresAuth && !currentUser) next('login');
    else if (!requiresAuth && currentUser) next('wallet');
    else next();
});

export default router;