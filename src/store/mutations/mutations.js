export default {
    SET_USERS_TO_STATE: (state, users) => {
        state.users = users;
    },
    SET_PRICE: (state, price) => {
        state.price = price;
    },
    SET_TOKEN: (state, token) => {
        state.token = token;
    },
    SET_LOGGED_IN(state, value) {
        state.user.loggedIn = value;
    },
    SET_USER(state, data) {
        state.user.data = data
    }
};