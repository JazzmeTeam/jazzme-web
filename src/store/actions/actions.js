export default {
    // SET_PRICE: (state, price) => {
    //   state.price = price;
    // },
    setprice(state, price) {
        state.price = price;
    },
    fetchUser({
        commit
    }, user) {
        commit("SET_LOGGED_IN", user !== null)

        if (user) {
            commit("SET_USER", {
                displayName: user.displayName,
                email: user.email
            })
        } else {
            commit("SET_USER", null)
        }
    }
};