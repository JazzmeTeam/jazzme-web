import axios from "axios";

const baseURL = "https://jazzme.org/service/v1/";
//USER
let users = "clients/all";
// let user = "clients/getUser";

// let config = {headers: {'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Credentials': true}, }


//OTHERS
let course = "coin/price";

export default {
    GET_USERS_FROM_API({ commit }) {
        return axios.get(baseURL + users).then(users => {
            commit("SET_USERS_TO_STATE", users.data);
            console.log("GET USERS INFO: ", users.data);
            return users;
        });
    },
    GET_PRICE_FROM_API({ commit }) {
        return axios.get(baseURL + course).then(price => {
            commit("setprice", price.data);
            return price;
        });
    }
};