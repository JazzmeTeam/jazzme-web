export default {
    USERS(state) {
        return state.users;
    },
    USER_ID: state => id => {
        return state.users.find(user => user.id == id);
    },
    PRICE(state) {
        return state.price;
    },
    GET_USER(state) {
        return state.user;
    },
    GET_TOKEN(state) {
        return state.token;
    },
    user(state) {
        return state.user
    }
};