# 🎷JAZZME Web

###### [app.jazzme.org](https://app.jazzme.org "app.jazzme.org") - doesn't working yet

#### This is the repository containing the code for the JAZZME App.

Full information about project you can get from the Telegram channels:

[@jazzqr - Main news channel](http://t.me/jazzqr "@jazzqr - Main news channel")

[@projector444 - Channel of project manager of the JAZZME](http://t.me/projector444 "@projector444 - Channel of project manager of the JAZZME")

#### Installation

1.  First of all, clone this project
    `git clone https://bitbucket.org/JazzmeTeam/jazzme-web.git`

2.  Installation
    `npm install`

3.  Run in dev-mode
    `npm run serve`

- Or build it
  `npm run build`


